# DevOps Final Project

### [Main App](https://gitlab.com/a.nurgazieva/devops-main-app) is just a web app that uses a recipe search [Tasty API](https://rapidapi.com/apidojo/api/tasty/). It's deployed using GitlabCI here: https://devops-main-app.herokuapp.com/

The Fallback App has the same core functionality except additional requests for similar recipe recommendations and can't query for a recipe by ID.
Fallback is here: https://devops-fallback-app.herokuapp.com/

![](images/screen1.png)

On the home page of the website, you can search for a recipe by the name of food or ingredient. For example, "pizza", "lentils".

![](images/screen2.png)

The main application sends a request to Tasty API with a user's query string and returns one of the recipes (if any). The recipe page renders the name, photo of the dish, nutritional value, and step-by-step cooking instructions.

![](images/screen3.png)
The difference between the Main App and the Fallback App is that the first one makes additional API call to render a block with recipes similar to the current one. All similar recipes are hyperlinks to detailed recipe page.

![](images/screen4.png)
Another route has been created to get the recipe by its ID (the Fallback App doesn't has it).

![](images/screen5.png)
Handling 404 errors...

![](images/screen6.png)
...and empty search results.


## Check fault tolerance:
```
vagrant up
```

```
vagrant ssh
```

```
kubectl get pods
```

Check the main app response:

```
curl webapp.mds
```

Scale down to zero:

```
kubectl scale --replicas=0 deployment main-app
```

Wait until all main-app pods are terminated.

Check the fallback app response:

```
curl webapp.mds
```

```
exit
```

```
vagrant destroy --force
```

![](images/screen7.png)